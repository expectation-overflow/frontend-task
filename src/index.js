import React from "react";
import ReactDOM from "react-dom/client";
import { ApolloProvider } from "@apollo/client";

import { client } from "./common/api";
import AppContextProvider from "./common/context/AppContextProvider";
import App from "./App";

// @ts-ignore
const root = ReactDOM.createRoot(document.getElementById("root"));
root.render(
  <React.StrictMode>
    <AppContextProvider>
      <ApolloProvider client={client}>
        <App />
      </ApolloProvider>
    </AppContextProvider>
  </React.StrictMode>
);
