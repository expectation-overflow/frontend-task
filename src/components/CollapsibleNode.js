import React, { useState, useContext, useEffect } from "react";

import When from "./When";
import AppContext from "../common/context/AppContext";

export default function CollapsibleNode({ item, className, children }) {
  const [isShown, setIsShown] = useState(false);
  //@ts-ignore
  const { isHidden } = useContext(AppContext);
  useEffect(() => {
    if (isHidden) setIsShown(false);
  }, [isHidden]);
  return (
    <ul className={className}>
      <li
        className={className}
        onClick={() => setIsShown(!isShown)}
        key={item.name}
      >
        {item.name}
      </li>
      <When condition={isShown && !isHidden}>
        <li>
          <ul className={className}>{children}</ul>
        </li>
      </When>
    </ul>
  );
}
