import { ApolloClient, InMemoryCache } from "@apollo/client";
import { URI } from "./constants";

export const client = new ApolloClient({
  uri: URI,
  cache: new InMemoryCache(),
});
