import { makeStyles } from "@material-ui/core/styles";

const useStyles = makeStyles(() => ({
  tree: {
    cursor: "pointer",
    display: "block",
    margin: "4px",
    padding: "2px",
    backgroundColor: "white",
    borderRadius: "8px",
    width: "55%",
  },
  parent: {
    border: "1px solid black",
  },
}));
export default useStyles;
