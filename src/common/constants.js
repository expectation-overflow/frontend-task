import { gql } from "@apollo/client";

export const KEYS = {
  CONTINENTS: "continents",
  COUNTRIES: "countries",
  LANGUAGES: "languages",
};

export const URI = "https://countries.trevorblades.com";

export const GET_CONTRIES = gql`
  query {
    continents {
      name
      countries {
        name
        languages {
          name
        }
      }
    }
  }
`;
