import React, { useState } from "react";

import AppContext from "./AppContext";

const AppContextProvider = ({ children }) => {
  const [isHidden, setIsHidden] = useState(false);
  const props = { isHidden: isHidden, setIsHidden: setIsHidden };
  return (
    <AppContext.Provider
      //@ts-ignore
      value={{ ...props }}
    >
      {children}
    </AppContext.Provider>
  );
};
export default AppContextProvider;
