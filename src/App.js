import React, { useContext } from "react";
import { useQuery } from "@apollo/client";

import AppContext from "./common/context/AppContext";
import CollapsibleNode from "./components/CollapsibleNode";
import { GET_CONTRIES } from "./common/constants";
// import data from "./common/data.json";
import useStyles from "./common/hooks/useStyles";

function App() {
  const { data } = useQuery(GET_CONTRIES);
  const styles = useStyles();

  //@ts-ignore
  const { isHidden, setIsHidden } = useContext(AppContext);
  const { continents } = data || {};

  return (
    <>
      {continents?.map((continent) => (
        <CollapsibleNode item={continent} className={styles.tree}>
          {continent.countries?.map((country) => (
            <li
              key={country.name}
              onClick={() => {
                if (country.languages.length === 0) {
                  setIsHidden(!isHidden);
                  setTimeout(() => setIsHidden(isHidden), 0);
                }
              }}
            >
              <CollapsibleNode item={country} className={styles.tree}>
                {country.languages?.map((language) => (
                  <li
                    key={language.name}
                    onClick={() => {
                      setIsHidden(!isHidden);
                      setTimeout(() => setIsHidden(isHidden), 0);
                    }}
                    className={styles.tree}
                  >
                    {language.name}
                  </li>
                ))}
              </CollapsibleNode>
            </li>
          ))}
        </CollapsibleNode>
      ))}
    </>
  );
}

export default App;
